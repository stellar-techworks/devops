#!/bin/sh

# Get new sshd configuration to support authorized keys from a remote source
# The remote source will be this git repo through the AuthorizedKeysCommand
# and wget or curl.

download_file(){
	curl -f -o "${1}" "${2}" || wget -O "${1}" "${2}"
}

mkdir downloads
REPO_URL="https://gitlab.com/stellar-techworks/devops/-/raw/master/provisioning"

# ec2-instance-connect borks the AuthorizedKeysCommand if it is installed
# https://github.com/aws/aws-ec2-instance-connect-config/issues/19
sudo apt-get -q -y remove ec2-instance-connect

# get AuthorizedKeysCommand and store it in the correct location
KEYS_CMD_URL="${REPO_URL}/getAuthorizedKeys.sh"
download_file downloads/getKeys "${KEYS_CMD_URL}"
# ownership and permissions must be set or it fails
# https://stackoverflow.com/a/27638306
sudo chmod 755 downloads/getKeys
sudo chown root:root downloads/getKeys
sudo mv downloads/getKeys /usr/local/bin

# get new config and put it in the correct location
SSHD_CONFIG_URL="${REPO_URL}/files/sshd_config"
download_file downloads/sshd_config "${SSHD_CONFIG_URL}"
sudo cp /etc/ssh/sshd_config /etc/ssh/sshd_config.orig
sudo mv -f downloads/sshd_config /etc/ssh/sshd_config

# restart sshd to use new config
sudo systemctl restart ssh
