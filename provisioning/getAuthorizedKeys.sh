#!/bin/sh

download_stdout(){
	curl -f "${1}" 2>/dev/null || 
		wget -q --output-document - "${1}" 2>/dev/null
}

KEYS_BASE_URL="https://gitlab.com/stellar-techworks/devops/-/raw/master/authorized_keys"

USER=$1

META_URL="http://169.254.169.254/latest/meta-data/tags/instance"
CODENAME_FIELD="Codename"

PROJECT=$(download_stdout ${META_URL}/${CODENAME_FIELD})

if [ -n "${PROJECT}" ]; then
	download_stdout "${KEYS_BASE_URL}/${PROJECT}/${USER}"
fi
download_stdout "${KEYS_BASE_URL}/${USER}"
