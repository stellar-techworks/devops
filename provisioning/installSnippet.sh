#!/bin/sh

# first download script
download_file(){
	curl -f -o "${1}" "${2}" || wget -O "${1}" "${2}"
}
download_file setupRemoteSSHKeys.sh \
	https://gitlab.com/stellar-techworks/devops/-/raw/master/provisioning/enableKeys.sh
# second, execute script
sh setupRemoteSSHKeys.sh
