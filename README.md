# Key Management

This is a set of tooling for managing deploy and login keys for multiple
servers.

## Installation

In your provisioning script, include these lines:

```bash
wget -O setupRemoteSSHKeys.sh \
	https://gitlab.com/stellar-techworks/devops/-/raw/master/provisioning/enableKeys.sh
sh setupRemoteSSHKeys.sh
```

## Adding your keys

Add a file named the same as a user account. It contains their authorized keys
in the same format as the `authorized_keys` file.

The file must be the same name as an *existing* user account. Trying to log in
to a nonexistent account will fail.

## Group logins

Some user accounts will be accessed by groups of real people, by having
multiple authorized keys in the list.

Login to the server with this syntax:

```bash
ssh username@hostname-or-ip-address
```

## Creating accounts done separately

Accounts must be created by another provisioning script. That script can call
this one to install remote key auth, but users must be created separately.

This login method only works on already existing users. This tool can't be
used to grant access, permissions, sudo membership, etc. This method only
provides remotely managed SSH keys.

## Notes on Key Management

1. [make curl return nothing on error](https://everything.curl.dev/http/response) 
2. [download gist](https://gist.github.com/mnebuerquo/e964c3f2c17c57c4894cb79575635bc1)
3. [aws ec2 instance connect must be removed](https://github.com/aws/aws-ec2-instance-connect-config/issues/19)
4. [Better SSH Authorized Keys Management](https://gist.github.com/sivel/c68f601137ef9063efd7)
